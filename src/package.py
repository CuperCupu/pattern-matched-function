name='Pattern Matched Function'
version='0.0.1'
author='Suhendi'
author_email='suhendi999@gmail.com'
url="https://bitbucket.org/CuperCupu/pattern-matched-function"
packages=['pfunc']
package_dir={
    "pfunc":"src/",
}
python_requires="~= 3.6.2"