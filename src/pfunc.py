from enum import Enum

class PatternError(Exception):
    pass

class Pattern:

    class SupportedType(Enum):
        IGNORE = 0
        ANY = 1
        BOOL = 2
        STR = 3 # TO DO
        INT = 4
        FLOAT = 5
        TUPLE = 6
        LIST = 7

    patterns = []

    @staticmethod
    def splittoken(pattern: str):
        res = []
        temp = ''
        open_par = ['(', '[']
        close_par = [')', ']']
        level = 0
        while pattern:
            t = pattern[0]
            pattern = pattern[1:]
            if t in open_par:
                level += 1
                temp += t
            elif t in close_par:
                level -= 1
                temp += t
            elif t == ' ' and level == 0:
                res += [temp]
                temp = ''
            elif level != 0:
                if t != ' ':
                    temp += t
            else:
                temp += t
        if temp:
            res += [temp]
        return res

    @staticmethod
    def parsetoken(token):
        retval = None
        if token:
            if isinstance(token, str):
                if token == '_':
                    retval = (Pattern.SupportedType.IGNORE, None)
                elif token[0] == '(' and token[len(token) - 1] == ')':
                    ntokens = Pattern.splittoken(token[1:][:-1].replace(',', ' '))
                    retval = (Pattern.SupportedType.TUPLE, Pattern.parsetoken(ntokens))
                elif token[0] == '[' and token[len(token) - 1] == ']':
                    ntokens = Pattern.splittoken(token[1:][:-1].replace(':', ' '))
                    retval = (Pattern.SupportedType.LIST, Pattern.parsetoken(ntokens))
                else:
                    try:
                        val = int(token)
                        return (Pattern.SupportedType.INT, val)
                    except ValueError:
                        try:
                            val = float(token)
                            retval = (Pattern.SupportedType.FLOAT, val)
                        except ValueError:
                            if token == '*':
                                retval = (Pattern.SupportedType.ANY, None)
                            elif token.lower() == 'true':
                                retval = (Pattern.SupportedType.BOOL, True)
                            elif token.lower() == 'false':
                                retval = (Pattern.SupportedType.BOOL, False)
            elif isinstance(token, list):
                res = []
                for t in token:
                    temp = Pattern.parsetoken(t)
                    if temp != None:
                        res += [temp]
                retval = res
        return retval

    @staticmethod
    def token_match(token, arg):
        matches = False
        (arg_type, arg_val) = token
        if arg_type == Pattern.SupportedType.ANY:
            matches = True if arg != None else False
        elif arg_type == Pattern.SupportedType.IGNORE:
            matches = True
        elif (arg_type == Pattern.SupportedType.INT and isinstance(arg, int)) or\
             (arg_type == Pattern.SupportedType.FLOAT and isinstance(arg, float)) or\
             (arg_type == Pattern.SupportedType.BOOL and isinstance(arg, bool)):
            matches = arg_val == arg
        elif (arg_type == Pattern.SupportedType.LIST and (isinstance(arg, list) or isinstance(arg, tuple))):
            if not arg_val:
                matches = False if arg else True
            elif len(arg_val) == 1 and arg_val[0][0] == Pattern.SupportedType.ANY:
                matches = True if len(arg) == 1 else False
            elif len(arg_val) == 1 and arg_val[0][0] == Pattern.SupportedType.IGNORE:
                matches = True if len(arg) == 1 else False
            else:
                last = None
                matches = True
                while matches and arg_val:
                    if not arg:
                        matches = False
                    else:
                        c_arg = arg[0]
                        arg = arg[1:]
                        c_tok = arg_val[0]
                        arg_val = arg_val[1:]
                        (last, _) = c_tok
                        matches = Pattern.token_match(c_tok, c_arg)
                if matches and arg and \
                   (not (last == Pattern.SupportedType.ANY or last == Pattern.SupportedType.IGNORE)):
                    matches = False
        elif (arg_type == Pattern.SupportedType.TUPLE and (isinstance(arg, list) or isinstance(arg, tuple))):
            if not arg_val:
                matches = False if arg else True
            else:
                last = None
                matches = len(arg) == len(arg_val)
                while matches and arg_val:
                    if not arg:
                        matches = False
                    else:
                        c_arg = arg[0]
                        arg = arg[1:]
                        c_tok = arg_val[0]
                        arg_val = arg_val[1:]
                        (last, _) = c_tok
                        matches = Pattern.token_match(c_tok, c_arg)
        return matches

    def match(self, args):
        pat = self.patterns[:]
        matches = len(pat) == len(args)
        while matches and pat:
            if not args:
                matches = False
            else:
                c_arg = args[0]
                args = args[1:]
                c_tok = pat[0]
                pat = pat[1:]
                matches = Pattern.token_match(c_tok, c_arg)
        return matches


    def __init__(self, pattern: str):
        patts = Pattern.splittoken(pattern)
        self.patterns = Pattern.parsetoken(patts)

class PatternFunc:

    def __init__(self, *args):
        self.patterns = []
        for arg in args:
            (pat, func) = arg
            self.register(pat, func)

    def register(self, pattern: str, func):
        pat = Pattern(pattern)
        self.patterns += [(pat, func)]

    @staticmethod
    def translate_args(patterns, args, catch_list=False):
        t_args = []
        i = 0
        while i < len(patterns):
            c_arg = args[i]
            (c_type, c_val) = patterns[i]
            if c_type == Pattern.SupportedType.ANY:
                if catch_list and i == len(patterns) - 1 and len(args) > 1:
                    t_args += [args[i:]]
                else:
                    t_args += [c_arg]
            elif c_type == Pattern.SupportedType.TUPLE:
                if c_val != None:
                    t_args += PatternFunc.translate_args(c_val, c_arg)
            elif c_type == Pattern.SupportedType.LIST:
                if c_val != None:
                    t_args += PatternFunc.translate_args(c_val, c_arg, True)
            i += 1
        return t_args

    def __call__(self, *args):
        func = None
        pattern = None
        i = 0
        while func == None and i < len(self.patterns):
            (p, f) = self.patterns[i]
            if p.match(args):
                func = f
                pattern = p
            i += 1
        if func != None:
            n_args = PatternFunc.translate_args(pattern.patterns, args)
            retval = None
            if hasattr(func, '__call__'):
                retval = func(*n_args)
            else:
                retval = func
            return retval
        else:
            raise PatternError("No matching arguments passed.")
