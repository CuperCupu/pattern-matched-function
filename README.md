# Pattern Matched Function

Inspired by the pattern matching from haskell. A PatternFunc class associates a pattern with a function. When invoking the PatternFunc class, it will try to match the arguments passed with any pattern registered by the class and invoke that function.

### Example

* Summing a list of integers

        sumList = PatternFunc(
            ("[*]", lambda x: x),
            ("[*:*]", lambda x, y: x + sumList(y))
        )

        l = [12, 35, 1, 2, 7, 10]
        print(sumList(l)) # Produces 67

* Basic list operation

        head = PatternFunc(
            ('[*]', lambda x: x), # One element
            ('[*:_]', lambda x: x), # Multiple Elements
        )
        tail = PatternFunc(
            ('[*]', []), # One element
            ('[_:*]', lambda x: x), # Multiple Elements
        )
        last = PatternFunc(
            ('[*]', lambda x: x), # One element
            ('[_:*]', lambda x: last(x)), # Multiple Elements
        )
        init = PatternFunc(
            ('[*]', []), # One element
            ('[*:*]', lambda x, y: [x] + init(y)), # Multiple Elements
        )
        l = [1, 2, 4, 7, 3]
        print(head(l)) # Produces 1
        print(tail(l)) # Produces [2, 4, 7, 3]
        print(last(l)) # Produces 3
        print(init(l)) # Produces [1, 2, 4, 7]

* Get every third elements of a list

        third = PatternFunc(
            ('()', []),
            ('(_)', []),
            ('(_,_)', []), # Note that parentheses were used instead of brackets
            ('[_:_:*]', lambda x: [head(x)] + third(tail(x))),
        )
        l = [1, 2, 4, 7, 3, 1, 10, 3, 10]
        print(third(l)) # Produces [4, 1, 10]

* Fibbonaci numbers

        fibbonaci = PatternFunc(
            ("0", 0),
            ("1", 1),
            ("*", lambda x: fibbonaci(x - 1) + fibbonaci(x - 2))
        )

        print(fibbonaci(21)) # Produces 10946

### Patterns

* Literals

    int literals are any string that can be casted into int, this means that '0' will be casted as int. To match floating-point, use '0.0'. This also apply for all int literals.

* Parentheses '()'

    Parentheses treats the argument as a tuple whose contents are seperated by ','.

* Backets '[]'

    Parentheses treats the argument as a list whose contents are seperated by ':'.

* Parentheses vs Brackets

    Parentheses treats the argument as a tuple while brackets treats the argument as a list eventhough they accept both tuple and list. This means that parentheses accepts exact sized tuple/list while brackets would match the rest of the tuple/list as a new list.

* Wildcard '*'

    Matches any value and pass it as an argument. The arguments are passed in the order from left to right.

* Ignore '_'

    Matches any value but is not assigned as an argument.